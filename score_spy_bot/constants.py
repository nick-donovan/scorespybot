# This file is part of ScoreSpyBot.
#
# ScoreSpyBot is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# ScoreSpyBot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with ScoreSpyBot. If not,
# see <https://www.gnu.org/licenses/>.

import os
import urllib.parse


class Constants:
    CREATOR_ID = os.environ["TELEGRAM_ID"]

    BOT_NAME = os.environ["SCORESPY_BOT_USERNAME"]
    BOT_TOKEN = os.environ["SCORESPY_BOT_TOKEN"]

    MONGO_USER = os.environ["MONGO_USER"]
    MONGO_PASS = os.environ["MONGO_PASS"]
    MONGO_CLUSTER = os.environ["SCORESPY_CLUSTER"]
    MONGO_DB = os.environ["SCORESPY_DB"]

    user = urllib.parse.quote_plus(MONGO_USER)
    password = urllib.parse.quote_plus(MONGO_PASS)
    MONGO_STR = f"mongodb+srv://{user}:{password}@{MONGO_CLUSTER}/?retryWrites=true&w=majority&appName={MONGO_DB}"
