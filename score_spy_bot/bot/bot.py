# This file is part of ScoreSpyBot.
#
# ScoreSpyBot is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# ScoreSpyBot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with ScoreSpyBot. If not,
# see <https://www.gnu.org/licenses/>.

from telegram import Update
from telegram.ext import (ApplicationBuilder, CommandHandler, CallbackQueryHandler, ChatMemberHandler, filters,
                          MessageHandler)

from score_spy_bot.bot.commands import start, show_help, submit, button, scores
from .status_handler import track_status, migrate, user_left_chat
from .error_handler import error_handler
from ..database.db import Db


class Bot:
    def __init__(self, token):
        self.token = token

    def start(self):
        application = ApplicationBuilder().token(self.token).build()
        Db.connect()

        no_edit = ~filters.UpdateType.EDITED_MESSAGE

        handlers = [
            MessageHandler(filters.StatusUpdate.LEFT_CHAT_MEMBER, user_left_chat),
            MessageHandler(filters.StatusUpdate.MIGRATE, migrate),
            CommandHandler('start', start),
            CommandHandler('help', show_help),
            CommandHandler('scores', scores, filters=no_edit),
            CommandHandler('submit', submit, filters=no_edit),
            ChatMemberHandler(track_status, ChatMemberHandler.MY_CHAT_MEMBER),
            CallbackQueryHandler(button)
        ]

        for handler in handlers:
            application.add_handler(handler)

        application.add_error_handler(error_handler)

        application.run_polling(allowed_updates=Update.ALL_TYPES)
