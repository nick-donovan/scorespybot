# This file is part of ScoreSpyBot.
#
# ScoreSpyBot is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# ScoreSpyBot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with ScoreSpyBot. If not,
# see <https://www.gnu.org/licenses/>.

from telegram import Update
from telegram.constants import ParseMode
from telegram.ext import ContextTypes


async def show_help(update: Update, context: ContextTypes.DEFAULT_TYPE):
    help_text = """Help for ScoreSpy:
    `/help` \- Show this\.
    `/submit Wordle <num> <score>` \- Add a score for today's Wordle\. 
    `/scores` \- Show scores for today's Wordle\.
    Use `<command> /help` for specific info about a command\."""

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=help_text,
        parse_mode=ParseMode.MARKDOWN_V2,
    )
