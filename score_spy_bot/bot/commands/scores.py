# This file is part of ScoreSpyBot.
#
# ScoreSpyBot is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# ScoreSpyBot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with ScoreSpyBot. If not,
# see <https://www.gnu.org/licenses/>.

import logging
from datetime import datetime

from telegram import Update
from telegram.constants import ParseMode
from telegram.ext import ContextTypes

from score_spy_bot import games
from score_spy_bot.bot.commands.misc import parse_date
from score_spy_bot.database import Operations

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)

scores_help = """Use /scores to view the leaderboard for today\! You can also specify a game and date too\.

Examples:
    `/scores Wordle <date>`: Show the scores for a specific date\.
    `/scores Wordle <num>`: Show the scores for a specific Wordle game\.
"""


async def scores(update: Update, context: ContextTypes.DEFAULT_TYPE):
    command_args = context.args

    if '/help' in command_args:
        await update.message.reply_text(
            text=scores_help,
            parse_mode=ParseMode.MARKDOWN_V2
        )
        return

    chat_id = update.effective_chat.id

    game_str = command_args[0] if len(command_args) == 2 else ""
    game = games.parse_game(game_str)

    date = parse_date(command_args[1], game) if len(command_args) == 2 else datetime.now()

    scores = Operations.get_chat_scores(chat_id, game.game_name, date)
    scores = sorted(scores, key=lambda x: x[1])

    if len(scores) == 0:
        if date.date() == datetime.now().date():
            msg = "No scores found for today!"
        else:
            msg = f"No scores found for {date.date()}"
    else:
        msg = "<u><b>Wordle scores for today</b></u>\n"
        for score in scores:
            msg = f"{msg}{score[0]} - {score[1]}/6\n"

    await context.bot.send_message(
        chat_id=chat_id,
        text=msg,
        parse_mode=ParseMode.HTML
    )
