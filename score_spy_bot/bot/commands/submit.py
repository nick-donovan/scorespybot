# This file is part of ScoreSpyBot.
#
# ScoreSpyBot is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# ScoreSpyBot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with ScoreSpyBot. If not,
# see <https://www.gnu.org/licenses/>.

import logging

from pymongo.errors import PyMongoError
from telegram import Update
from telegram.constants import ReactionEmoji, ParseMode
from telegram.ext import ContextTypes

from score_spy_bot import games
from score_spy_bot.bot.commands.misc import get_help_msg, parse_date

from score_spy_bot.database import Operations
from score_spy_bot.user import User

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)

# Other games aren't officially supported yet, leaving that info out for now.
add_help = """Use /submit to submit a score\! The easiest way is to *copy/paste from Wordle* and prepend /submit, 
but there are other ways too\.

Examples: 
    `/submit <num> <score>`: Add score for specific Wordle game

    `/submit <date> <score>`: Add score for a specific date

    `/submit Wordle <num> <score>`: Same as the "share" copy/pase
"""


async def submit(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """Handler for the /submit command used to add a new user score."""
    if '/help' in context.args:
        await update.message.reply_text(
            text=add_help,
            parse_mode=ParseMode.MARKDOWN_V2
        )
        return

    command_args = context.args

    tg_id = update.effective_user.id
    tg_username = update.effective_user.username
    tg_first_name = update.effective_user.first_name
    chat_id = update.effective_chat.id

    try:
        date, game, score = parse_score_data(command_args)

        logger.info(f"Adding {game.game_name} score of {score} for {tg_id} on {date.date()}")

        user = User(tg_id, tg_username, tg_first_name)
        Operations.new_score(user, chat_id, game.game_name, score, date)

        await update.message.set_reaction(ReactionEmoji.THUMBS_UP)

    except ValueError as e:
        await update.message.reply_text(str(e))
    except PyMongoError:
        await update.message.reply_text("Hmm... I couldn't save your score, try again?")


def parse_score_data(command_args):
    """Return the score data from the command"""

    if len(command_args) < 2:
        raise ValueError(get_help_msg)
    elif len(command_args) == 2:
        game = games.default_game
        date = parse_date(command_args[0], game)
        score = game.get_score(command_args[1])
    else:
        game = games.parse_game(command_args[0])
        date = parse_date(command_args[1], game)
        score = game.get_score(command_args[2])

    return date, game, score