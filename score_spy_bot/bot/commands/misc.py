# This file is part of ScoreSpyBot.
#
# ScoreSpyBot is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# ScoreSpyBot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with ScoreSpyBot. If not,
# see <https://www.gnu.org/licenses/>.

import logging

from datetime import datetime
from re import match
from telegram import Update
from telegram.ext import ContextTypes

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)

get_help_msg = "That doesn't look right! Try looking at /help!"


async def button(update: Update, context: ContextTypes.DEFAULT_TYPE):
    query = update.callback_query
    await query.answer()
    # await query.edit_message_text(text=f"Selected: {query.data}")


def parse_date(date_or_game_num: str, game):
    """Parse and return date from the add command using the game's number, the provided date, or the current date."""
    try:
        date_pattern = r"^(\d{1,2})[/-](\d{1,2})[/-](\d{2,4})$"
        if match(date_pattern, date_or_game_num):
            date_or_game_num.replace('-', '/')
            try:
                return datetime.strptime(date_or_game_num, "%d/%m/%y")
            except ValueError:
                return datetime.strptime(date_or_game_num, "%d/%m/%Y")

        else:
            game_num = int(date_or_game_num)
            return game.game_num_to_utc(game_num)
    except ValueError:
        raise ValueError(get_help_msg)
