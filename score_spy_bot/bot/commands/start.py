# This file is part of ScoreSpyBot.
#
# ScoreSpyBot is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# ScoreSpyBot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with ScoreSpyBot. If not,
# see <https://www.gnu.org/licenses/>.

import logging

from pymongo.errors import PyMongoError
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ContextTypes

from score_spy_bot.database import Operations

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    source = [  # https://github.com/python-telegram-bot/python-telegram-bot/blob/master/examples/inlinekeyboard.py
        [
            InlineKeyboardButton("Source", url="https://gitlab.com/nick-donovan/scorespybot")
        ],
    ]

    chat_id = update.effective_chat.id

    try:
        Operations.new_start(chat_id)
        logger.info(f"Added {chat_id} to the 'chats' collection")
        await context.bot.send_message(
            chat_id=chat_id,
            text="Welcome to ScoreSpy! Look at /help to get started!",
            reply_markup=InlineKeyboardMarkup(source)
        )
    except PyMongoError:
        await context.bot.send_message(
            chat_id=chat_id,
            text="I'm sorry, I couldn't add your chat to the database... Try again?",
        )
        raise ValueError(f"Couldn't add {chat_id} to 'chats' collection")


