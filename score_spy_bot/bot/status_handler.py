# This file is part of ScoreSpyBot.
#
# ScoreSpyBot is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# ScoreSpyBot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with ScoreSpyBot. If not,
# see <https://www.gnu.org/licenses/>.

import logging
import telegram.error

from telegram import ChatMember, ChatMemberUpdated, Update
from telegram.constants import ChatType
from telegram.ext import ContextTypes

from score_spy_bot.database import Operations

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)


def extract_status_change(chat_member_update: ChatMemberUpdated):
    """Takes a ChatMemberUpdated instance and extracts whether the 'old_chat_member' was a member
    of the chat.py and whether the 'new_chat_member' is a member of the chat.py. Returns None, if
    the status didn't change.
    """
    status_change = chat_member_update.difference().get("status")
    old_is_member, new_is_member = chat_member_update.difference().get("is_member", (None, None))

    if status_change is None:
        return None, None

    old_status, new_status = status_change
    was_member = old_status in [
        ChatMember.MEMBER,
        ChatMember.OWNER,
        ChatMember.ADMINISTRATOR,
    ] or (old_status == ChatMember.RESTRICTED and old_is_member is True)
    is_member = new_status in [
        ChatMember.MEMBER,
        ChatMember.OWNER,
        ChatMember.ADMINISTRATOR,
    ] or (new_status == ChatMember.RESTRICTED and new_is_member is True)

    return was_member, is_member


async def track_status(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Tracks when the bot joins or leaves a group or chat"""
    chat = update.effective_chat

    if chat.type != ChatType.GROUP and chat.type != ChatType.SUPERGROUP:
        try:
            logger.info(f"Chat {chat.title} is a {chat.type}. Leaving chat.")
            await chat.leave()
        except telegram.error.Forbidden:
            logger.info(f"Left {chat.title}.")

        return

    was_member, is_member = extract_status_change(update.my_chat_member)
    cause_name = update.effective_user.username

    if not was_member and is_member:
        logger.info(f"{cause_name} added the bot to the group {chat.title}: {chat.id}.")

    elif was_member and not is_member:
        logger.info(f"{cause_name} removed the bot from the group {chat.title}: {chat.id}.")


async def migrate(update: Update, context: ContextTypes.DEFAULT_TYPE):
    old_id = update.message.migrate_from_chat_id
    new_id = update.effective_chat.id
    if not old_id:
        raise ValueError("Old ID not present when attempting to migrate")
    if not new_id:
        raise ValueError("New ID not present when attempting to migrate")

    if not Operations.change_chat_id(old_id, new_id):
        raise ValueError(f"Group migrated but ID was not updated: {old_id} to {new_id}")


async def user_left_chat(update: Update, context: ContextTypes.DEFAULT_TYPE):
    print("user_left")
    print(update)
