# This file is part of ScoreSpyBot.
#
# ScoreSpyBot is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# ScoreSpyBot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with ScoreSpyBot. If not,
# see <https://www.gnu.org/licenses/>.

from datetime import datetime, timezone


from score_spy_bot.database.crud import Chats, Users, Scores
from score_spy_bot.database.db import Db
from score_spy_bot.user import User


def sanitize_date(date: datetime) -> float:
    return date.astimezone(tz=timezone.utc).replace(hour=0, minute=0, second=0, microsecond=0).timestamp()


class Operations:
    db = Db.get_db()
    chats = Chats(db)
    users = Users(db)
    scores = Scores(db)

    @classmethod
    def new_start(cls, chat_id):
        return cls.chats.insert_chat(chat_id)

    @classmethod
    def new_score(cls, user: User, chat_id: int, game_name: str, score, date: datetime):
        date = sanitize_date(date)

        # Mongo Operations are atomic and the order of these aren't of a concern. No session is needed.
        # Add user if they don't exist.
        cls.users.insert_user(user)

        # Add user to the chat, creating the chat if it hasn't been `/start`d.
        cls.chats.add_user_to_chat(chat_id, user.tg_id)

        # Finally insert the score
        cls.scores.insert_score(user.tg_id, game_name, score, date)

    @classmethod
    def get_chat_scores(cls, chat_id: int, game_name: str, date: datetime):
        date = sanitize_date(date)

        # Get users in chat
        users = cls.chats.get_members(chat_id)

        scores = []
        for user_id in users:
            user = cls.users.get_user(user_id)
            score = cls.scores.get_score(user_id, game_name, date)
            if score and user:
                scores.append((user.tg_first_name, score["score"]))
        return scores

    @classmethod
    def change_chat_id(cls, old_id: int, new_id: int):
        return cls.chats.change_chat_id(old_id, new_id)
