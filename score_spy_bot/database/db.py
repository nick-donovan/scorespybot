# This file is part of ScoreSpyBot.
#
# ScoreSpyBot is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# ScoreSpyBot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with ScoreSpyBot. If not,
# see <https://www.gnu.org/licenses/>.

import logging

import pymongo
from pymongo.errors import PyMongoError
from pymongo.database import Database
from pymongo.mongo_client import MongoClient

from score_spy_bot import Constants

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)


class Db:
    _client: MongoClient = None
    _db: Database = None
    _timeout_ms = 5000

    @classmethod
    def get_db(cls):
        if cls._db is None:
            cls.connect()

        return cls._db

    @classmethod
    def connect(cls):
        # Connect to client
        cls._connect_client()

        # Retrieve database
        cls._db = cls._client[Constants.MONGO_DB.lower()]

        # Create indices
        cls.create_indices()

    @classmethod
    def create_indices(cls):
        scores = cls._db.scores

        # Ensure uniqueness of scores, faster lookup
        scores.create_index([
            ("tg_id", pymongo.ASCENDING),
            ("date", pymongo.ASCENDING),
            ("game", pymongo.ASCENDING),
        ], unique=True)

    @classmethod
    def _connect_client(cls):
        try:
            if cls._client is None:
                cls._client = MongoClient(
                    Constants.MONGO_STR,
                    connectTimeoutMS=cls._timeout_ms,
                    socketTimeoutMS=cls._timeout_ms,
                    serverSelectionTimeoutMS=cls._timeout_ms
                )
                cls._client.server_info()
                logger.info("Connected to Mongo database.")
        except pymongo.errors.ConnectionFailure | pymongo.errors.ConnectionFailure as e:
            logger.fatal(f"Could not connect to MongoDB: {e}")
            exit(1)
