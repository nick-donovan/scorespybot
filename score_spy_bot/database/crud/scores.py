# This file is part of ScoreSpyBot.
#
# ScoreSpyBot is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# ScoreSpyBot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with ScoreSpyBot. If not,
# see <https://www.gnu.org/licenses/>.

import logging

import pymongo.errors

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)


class Scores:
    def __init__(self, db):
        self.db = db

    def insert_score(self, tg_id: int, game_name: str, score, date: float):
        db_score = {
            "tg_id": tg_id,
            "game": game_name,
            "score": score,
            "date": date
        }

        try:
            self.db.scores.insert_one(db_score)
        except pymongo.errors.DuplicateKeyError:
            raise ValueError("This score is already present!")

    def get_score(self, tg_id: int, game_name: str, date: float):
        query = {
            "tg_id": tg_id,
            "game": game_name,
            "date": date
        }
        scores = self.db.scores
        return scores.find_one(query)
