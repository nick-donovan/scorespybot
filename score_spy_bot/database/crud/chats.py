# This file is part of ScoreSpyBot.
#
# ScoreSpyBot is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# ScoreSpyBot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with ScoreSpyBot. If not,
# see <https://www.gnu.org/licenses/>.

import logging

import pymongo.errors

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)


class Chats:
    def __init__(self, db):
        self.db = db

    def insert_chat(self, chat_id: int):
        chats = self.db.chats
        chat = {
            "_id": chat_id,
            "members": []
        }

        try:
            chats.insert_one(chat)
        except pymongo.errors.DuplicateKeyError:
            pass

    def add_user_to_chat(self, chat_id: int, telegram_id: int):
        chats = self.db.chats

        if not chats.find_one({"_id": chat_id}):
            self.insert_chat(chat_id)

        chats.update_one(
            {"_id": chat_id},
            {"$addToSet": {"members": telegram_id}}
        )

    def get_members(self, chat_id: int):
        chats = self.db.chats
        members = chats.find_one(
            {"_id": chat_id},
            {"_id": 0, "members": 1}
        )
        return members['members']

    def change_chat_id(self, old_id: int, new_id: int) -> bool:
        chats = self.db.chats

        result = chats.update_one(
            {"_id": old_id},
            {"$set": {"_id": new_id}},
        )
        return result.modified_count == 1
