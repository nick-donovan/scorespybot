# This file is part of ScoreSpyBot.
#
# ScoreSpyBot is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# ScoreSpyBot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with ScoreSpyBot. If not,
# see <https://www.gnu.org/licenses/>.

import logging

import pymongo.errors

from score_spy_bot.user import User

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)


class Users:
    def __init__(self, db):
        self.db = db

    def insert_user(self, user: User):
        db_user = {
            "_id": user.tg_id,
            "username": user.tg_username,
            "first_name": user.tg_first_name
        }

        try:
            self.db.users.insert_one(db_user)
        except pymongo.errors.DuplicateKeyError:
            # Change the user's username and name if they're different.
            db_user = self.db.users.find_one({"_id": user.tg_id})
            if db_user["username"] != user.tg_username or db_user["first_name"] != user.tg_first_name:
                self.db.users.update_one(
                    {"_id": user.tg_id},
                    {"$set":
                        {
                            "username": user.tg_username,
                            "first_name": user.tg_first_name
                        }
                    }
                )

    def get_user(self, user_id: int):
        user = self.db.users.find_one({"_id": user_id})
        if user:
            return User(user["_id"], user["username"], user["first_name"])
        else:
            return None
