# This file is part of ScoreSpyBot.
#
# ScoreSpyBot is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# ScoreSpyBot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with ScoreSpyBot. If not,
# see <https://www.gnu.org/licenses/>.

import re

from datetime import datetime, timezone, timedelta
from zoneinfo import ZoneInfo

from .game import Game


class Wordle(Game):
    # f"https://www.nytimes.com/svc/wordle/v2/{date}.json"
    game_name = "Wordle"
    game_regex = r"[Ww]ordle"
    min_score, max_score = 0, 6

    game_tz = ZoneInfo("America/New_York")
    start_date = datetime(year=2021, month=6, day=19, tzinfo=game_tz)

    @staticmethod
    def game_num_to_utc(num: int):
        d = Wordle.start_date + timedelta(days=num)
        return d.astimezone(tz=timezone.utc)

    @staticmethod
    def get_score(score_str: str):
        score_pattern = r"^([xX\d])(/6)?$"

        match = re.match(score_pattern, score_str)
        if not match:
            raise ValueError(f"Invalid score for {Wordle.game_name}: {score_str}")

        score = match.group(1).lower().replace('x', '0')
        score = int(score)

        if not (Wordle.min_score <= score <= Wordle.max_score):
            raise ValueError(f"Score provided out of range for {Wordle.game_name}.")

        return score
