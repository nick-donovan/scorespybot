# This file is part of ScoreSpyBot.
#
# ScoreSpyBot is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# ScoreSpyBot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with ScoreSpyBot. If not,
# see <https://www.gnu.org/licenses/>.

import re

from typing import Type

from .game import Game
from .wordle import Wordle

supported_games = [
    Wordle
]

default_game = Wordle


def parse_game(s: str) -> Type[Game]:
    """Return the game type from the given string or the default one."""
    for game in supported_games:
        if re.match(game.game_regex, s):
            return game

    # todo raise default game used
    return default_game
