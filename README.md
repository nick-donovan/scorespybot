# ScoreSpyBot

ScoreSpy is a telegram bot that tracks Wordle scores! You can create leaderboards, challenge your friends, and keep track of progress over time!

## Usage
To use @scorespybot:
1. Add the bot to your group chat
2. Look at the `/help` command.
	- Each command has its own `/help`, so you can use `/submit /help` to view help for the `/submit` command.
3. Use `/start` or simply `/submit` a Wordle score. The easiest way to do this is to use the "Share" function from Wordle itself and paste it after the command `/submit`

	 ```
	/submit Wordle 996 1/6
	🟩🟩🟩🟩🟩
	```

4. To view the scores use the `/scores` command such as `/scores Wordle 996`

## Roadmap
- [ ] Add support for chat UTC offsets.
- [ ] Add documentation for self-hosting
- [ ] Synchronize user scores across several chats
	- This is a challenge due to how Telegram handles bot APIs

## License
ScoreSpyBot is licensed under the GPL version 3 or later. See the [COPYING](https://www.gnu.org/licenses/gpl-3.0.txt) file for more information.
